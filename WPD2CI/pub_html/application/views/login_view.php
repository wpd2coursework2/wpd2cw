<!DOCTYPE html>
<html lang="en">
<head>
	<title>AJA Online Shop - Login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="<?php echo base_url();?>assets/css/shopLogin.css" rel="stylesheet" type="text/css">


	<meta name="msapplication-TileColor" content="#FFFFFF">

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,600" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="row content">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-4">
				</div>
				<div class="col-sm-4">
                    <div class="centerImg">
						<img class="logInImg" src="<?php echo base_url();?>assets/images/logos/logo-aja-online.png">
					</div>
					<div class="well">
						<div class="form">
                            <?php echo validation_errors();?>
                            <?php echo form_open('CheckCredentials'); ?>
							<?php echo $this->session->flashdata('verify_msg'); ?>
                            <div class="formItem">
                                <label class="formLabel" for="username"><b>Username </b></br></label>
                                <input type="text" placeholder="Enter Username" name="username">
                            </div>
							<div class="formItem">
                                <label class="formLabel" for="pwd"><b>Password </b></br></label>
                                <input type="password" placeholder="Enter Password" name="pwd">
				            </div>
				            <div class="formItem">
								</br>
                                <button type="submit">Login</button>
                            </div></form><br/>
							<a href="<?php echo base_url('index.php/Register')?>">Create an Account</a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
</body>
</html>
