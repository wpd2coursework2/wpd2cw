<!DOCTYPE html>
<html lang="en">
<head>
	<title>AJA Online Shop - Register</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="<?php echo base_url();?>assets/css/shopReg.css" rel="stylesheet" type="text/css">


	<meta name="msapplication-TileColor" content="#FFFFFF">

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,600" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="row content">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-4">
				</div>
				<div class="col-sm-4">
                    <div class="centerImg">
						<img class="logInImg" src="<?php echo base_url();?>assets/images/logos/logo-aja-online.png">
					</div>
					<?php echo $this->session->flashdata('msg'); ?>
					<?php echo $this->session->flashdata('msg_username'); ?>
					<?php echo $this->session->flashdata('msg_email'); ?>
					<div class="well">
						<div class="form">
                            <?php echo validation_errors();?>
                            <?php echo form_open('Register'); ?>
                            <div class="formItem">
                                <label class="formLabel" for="userName"><b>Username</b></br></label>
								</br>
                                <input type="text" placeholder="Enter Username" name="userName">
                            </div>
							<div class="formItem">
                                <label class="formLabel" for="fullName"><b>Full Name</b></br></label>
                                </br><div></div><input type="text" placeholder="Input your full name" name="fullName">
                            </div>
							<div class="formItem">
                                <label class="formLabel" for="emailAddress"><b>E-Mail </b></br></label>
								</br>
                                <input type="text" placeholder="Input E-mail address" name="emailAddress">
                            </div>
							<div class="formItem">
                                <label class="formLabel" for="passA"><b>Password</b></br></label>
								</br>
                                <input type="password" placeholder="Enter Password" name="passA">
				            </div>
							<div class="formItem">
                                <label class="formLabel" for="passB"><b>Confirm</b></br></label>
								</br>
                                <input type="password" placeholder="Confirm Password" name="passB">
				            </div>
				            <div class="formItem">
                                <button type="submit">Submit</button>								
                            </div>
							<?php echo form_close(); ?>
                        </div>
                    </div>

                </div>
            </div>
		</div>
	</div>
</div>
</body>
</html>
