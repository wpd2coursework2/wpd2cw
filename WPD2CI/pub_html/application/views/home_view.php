<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Store</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/Stylesheet.css" rel="stylesheet">


</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">AJA Online Store</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					 <li>
                        <a href="<?php echo base_url() ?>index.php/Home/logout">Logout</a>
                    </li>
                </ul>
				 <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url();?>index.php/Wishlist">Wish List</a>
                    </li>
                    <li>
                        <a href="#">Shopping Cart</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
		
            <h2>Welcome <b><?php echo $fullName;?></b> to our online web shop
			<img class="logInImg" src="<?php echo base_url();?>assets/images/logos/logo-aja-online.png"></h2>
            <p>Welcome to the AJA Online Technology store. 
			Our store specialises in selling Games Consoles as well as 
			VR Headsets, Laptops and Mobile Phones!</p>			

        </header>

        <hr>
		
        <!-- Title -->
        <div class="row">
		<?php echo $this->session->flashdata('wish_fail');?>
		<?php echo $this->session->flashdata('wish_success');?>
            <div class="col-lg-12">
                <h3>All Items</h3>
            </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        <div class="row text-center">
			<?php
			foreach($itemList as $row) { ?>
            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="<?php echo $row['image'];?>" alt="">
                    <div class="caption">
                        <h3><?php echo $row['itemName'];?></h3>
                        <p><?php echo $row['shortDesc'];?></p>
                        <p>
                            <a href="#" class="btn btn-primary">Buy Now!</a>
							<div></div>
							<a href="<?php echo base_url();?>index.php/Wishlist/addTo/<?php echo $row['ItemID'];?>" class="btn btn-default">Add to Wish List</a>
                        </p>
						<p> Price: £<?php echo $row['price'];?></p>
                    </div>
                </div>
            </div>
			<?php }?>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; AJA 2017</p>
                </div>
				
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
