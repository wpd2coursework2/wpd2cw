<?php
class Wishlist extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('user');
		$this->load->model('items');		
	}

	
    public function index()
    {
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['userName'] = $session_data['userName'];			
			$data['userEmail'] = $session_data['userEmail'];
			$data['fullName'] = $session_data['fullName'];
			$data['itemList'] = $this->items->populateItems();
			$data['wishList'] = $this->items->populateWishes($session_data['userName']);
			$this->load->view('wishlist_view', $data);			

		}
		else
		{
			redirect('login', 'refresh');
		}
	}
	
	public function removeFrom($wishId)	
	{		
		$this->items->removeFromWishlist($wishId);
		$this->session->set_flashdata('wish_success','<div class="alert alert-success text-center">Item successfully removed</div>');
		redirect('wishlist');
	}
	
	public function addTo($itemId)
	{
		$itemID = $this->uri->segment(3);
		
		$itemArray = $this->items->getItemDetails($itemID);
		
		if($itemArray)
		{
			$data = array();
			$today = date("F j, Y, g:i a");
			$session_data = $this->session->userdata('logged_in');
			foreach($itemArray as $row)
			{
				$data = array(
				'userName' => $session_data['userName'],
				'dateAdded' => $today,
				'image' => $row->image,
				'shortDesc' => $row->shortDesc,
				'longDesc' => $row->longDesc,
				'price' => $row->price,
				'quantity' => $row->quantity,
				'itemName' => $row->itemName,
				'ItemId' => $row->ItemID);
			}
			if($this->items->wishExists($itemId, $session_data['userName'])){
			$this->session->set_flashdata('wish_fail','<div class="alert alert-danger text-center"><b>' . $data['itemName'] . '</b> already exists in your wishlist!</div>');
			redirect('home');
			}
			else
			{
			$this->items->addToWishlist($data);
			$this->session->set_flashdata('wish_success','<div class="alert alert-success text-center">Successfully added <b>' . $data['itemName'] . '</b> to your wishlist!</div>');
			redirect('home');
			return TRUE;
			}
			
		}
		else
		{
			return FALSE;
		}		
		
	}
		
}