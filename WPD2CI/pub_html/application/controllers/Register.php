<?php

date_default_timezone_set('Europe/London');

class Register extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->database();
		$this->load->model('User');
		
		
	}

	function index()
	{
		
		$this->doRegister();
	
	}
	
	function doRegister()
	{
		//create form validation rules
		
		$this->form_validation->set_rules('userName', 'Username', 'trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('fullName', 'Full Name', 'trim|required|min_length[5]|max_length[25]');
		$this->form_validation->set_rules('emailAddress', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('passA', 'Password', 'trim|required|matches[passB]');
		$this->form_validation->set_rules('passB', 'Confirm Password', 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('register_view');
		}
		else
		{
			$salt = $this->User->generate_salt();
			
			//on success, we will add the user details and salt to the database
			
			$data = array(
			'userName' => $this->input->post('userName'),
			'fullName' => $this->input->post('fullName'),
			'userEmail' => $this->input->post('emailAddress'),
			'pwdSalt' => $salt,
			'pwdHash' => password_hash($salt.$this->input->post('passA'), PASSWORD_BCRYPT));
			
			if(!$this->User->matchUsername($this->input->post('userName')))
			{			
				if (!$this->User->matchEmail($this->input->post('emailAddress')))
				{					/*----Verification email----*/
					if($this->User->addUser($data))
					{
						if($this->User->verifyReg($this->input->post('emailAddress')))
						{	
							$this->session->set_flashdata('msg','<div class="alert alert-success text-center">Account created. Please check the email we sent to <strong>' . $this->input->post('emailAddress') . ' </strong>to activate your account!</div>');
					
							redirect('Register/doRegister');
						}
						else
						{
							//error 1 - unknown
							$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">There is an issue verifying your e-mail address. Please try again later!</div>');
					
							redirect('Register/doRegister');				
						}
					}
					else
					{	//error 2 - email in use
					$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">An unknown error occurred! Please try again</div>');
					redirect('Register/doRegister');				
					}
				}
				else{
					$this->session->set_flashdata('msg_email','<div class="alert alert-danger text-center">That e-mail address is already in use!</div>');
				    redirect('Register/doRegister');
					
				}
			}
			else{			
				$this->session->set_flashdata('msg_username','<div class="alert alert-danger text-center">The username you entered is already registered!</div>');
				redirect('Register/doRegister');
				
			}
		}
	}
		
		function verify($hash=NULL)
		{
			if ($this->User->verifyEmailWithKey($hash))
			{
				$this->session->set_flashdata('verify_msg', '<div class="alert alert-success text-center">You have successfully verified your email. Your account is now active and you may login!</div>');
				
				redirect('login');
				
			}
		}		
	}