<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class CheckCredentials extends CI_Controller {
	
	function __construct() 	
	{
		parent::__construct();
		$this->load->model('user','',TRUE);
	}
	
	function index()
	{
		$this->load->library('form_validation'); //import CodeIgniter form validation library
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('pwd', 'Password', 'trim|required|callback_check_database');
	
	
	   if($this->form_validation->run() == FALSE)
   {
     //unable to verify the credentials, so redirecting back to login page
     $this->load->view('login_view');
   }
   else
   {
     //credentials matching a user found on our database, so the user is directed to the shop
     redirect('home', 'refresh');
   }
	}
	
 	
	function check_database($pwd)
	{
	 
	 $username = $this->input->post('username');
	 
	 $result = $this->user->doLogin($username, $pwd);
	 
	 //if we find a user with these login details, create a session for that user
	 
	 if($result)
	 {
		 $sess_array = array(); //instantiate an array holding all current user info
		 foreach($result as $row)
		 {
		  $sess_array = array(
		 'UserID' => $row->UserID,
		 'userName' => $row->userName,
		 'userEmail' => $row->userEmail,		
		 'fullName' => $row->fullName
		 );
		 
		 $this->session->set_userdata('logged_in', $sess_array);		 
		 }
		return TRUE;	
	 }
	 else
	 {
		 $this->form_validation->set_message('check_database', 'Invalid username or password, try again');
		 return false;
	 
		}
 
		 
		 
		 
	}
}


