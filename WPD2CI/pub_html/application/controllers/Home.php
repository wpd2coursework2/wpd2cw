<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('user');
		$this->load->model('items');
	}
	
	function index()
	{ 	
	
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
			$data['userName'] = $session_data['userName'];			
			$data['userEmail'] = $session_data['userEmail'];
			$data['fullName'] = $session_data['fullName'];
			$data['itemList'] = $this->items->populateItems();
			
			$this->load->view('home_view', $data);			

		}
		else
		{
			redirect('login', 'refresh');
		}
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('home', 'refresh');
		 
	}
	
	}
	
	

