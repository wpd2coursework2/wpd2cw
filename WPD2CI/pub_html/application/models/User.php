<?php

date_default_timezone_set('Europe/London');

Class User extends CI_Model
{
	function doLogin($userName, $pwd)
	{
		$salt = $this->retrieve_salt($userName);
		$tryHash = $this->retrieve_hash($userName);
		$loginquery = '';
		
		if(password_verify($salt.$pwd, $tryHash))
		{
			$this->db-> select('UserID, userEmail, userName, fullName, pwdHash, status');
			$this->db-> from('users');
			$this->db-> where('userName', $userName);
			$this->db-> where('status', "Active");
			$this->db-> limit(1);
		
			$loginquery = $this->db->get();				
		}
		else
		{ 
		return FALSE; 
		}
		if($loginquery -> num_rows()==1)
		{
			return $loginquery->result(); 
		}
	}

	
	function addUser($data)
	{
		return $this->db->insert('users', $data);
		
	}
	
	function generate_salt($length = 12)
	{
		$source = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890£&*';
			$i=0;
			$usalt = '';
				while($i<$length)
				{
					$usalt.=substr($source,rand(1,strlen($source)),1);
					$i+=1;
				}
      return $usalt;
    }
	
	function retrieve_salt($username)
	{
		$query = $this->matchUsername($username);
		//$hash = '';
		if($query) 
		{
			foreach($query as $row)
			{
			$salt = $row['pwdSalt'];
			}			
			return $salt;
		}
		else
		{
			return false;
		}		
	}

	function retrieve_hash($username)
	{
		$query = $this->matchUsername($username);
		//$hash = '';
		if($query) 
		{
			foreach($query as $row)
			{
			$hash = $row['pwdHash'];
			}			
			return $hash;
		}
		else
		{
			return false;
		}		
	}
	
	
	//we need to verify the email address is valid, so send verification email
	
	function verifyReg($to_userEmail)
	{		
		//construct email
		
		$from = 'jackbeanstalk52@gmail.com';
		$subject = "AJA Online Web Store Registration - Verification";
		$message = 'Hi there, this email was used to register for our web store<br /> Please ignore this email if you did not expect it, otherwise please click the link below to activate your account:<br /><br />http://ec2-52-56-221-162.eu-west-2.compute.amazonaws.com/WPD2CI/pub_html/index.php/Register/verify/' .md5($to_userEmail) . '<br /><br /><br />Thanks! <br /> AJA Team;';
		
		//email config
		
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com'; //smtp host name
        $config['smtp_port'] = '465'; //gmail
        $config['smtp_user'] = $from;
        $config['smtp_pass'] = 'beanstalk123'; //$from password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->email->initialize($config);
        
        //send mail
		
        $this->email->from($from, 'AJA Online WebStore');
        $this->email->to($to_userEmail);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
		
		
	}
	
	function verifyEmailWithKey($key)
	{
		$data = array('status' => "Active");
		$this->db->where('md5(userEmail)', $key);
		return $this->db->update('users', $data);
	}

	
	function populateUsers()
	{
		$session_data = $this->session->userdata('logged_in');		
		{ 	//get all users
			$this->db->select('*');
			$this->db->from('users');
			$userquery = $this->db->get();
			if($userquery->num_rows() > 0)
			{
			return $userquery->result_array();
			}				
			return array();
		}
	}
	

	function matchUsername($username)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('userName', $username);
		$this->db->limit(1);
		
		$matchquery = $this->db->get();
		
		if ($matchquery -> num_rows()==1)
		{
			return $matchquery->result_array();			
		}
		else
		{
			return false;
		}
	}
	
	
	
	function matchEmail($email)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('userEmail', $email);
		$this->db->limit(1);
		
		$matchquery = $this->db->get();
		
		if ($matchquery -> num_rows()==1)
		{
			return $matchquery->result_array();
			
		}
		else
		{
			return false;
		}
	}

}

	
