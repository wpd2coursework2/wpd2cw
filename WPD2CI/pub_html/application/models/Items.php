<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Items extends CI_Model
{
	function populateItems()
	{
		// $session_data = $this->session->userdata('logged_in');		
		{ 	//get all items and details pertaining to them, so that we can foreach them onto the page
			$this->db->select('*');
			$this->db->from('items');
			$itemquery = $this->db->get();
			if($itemquery->num_rows() > 0)
			{
				return $itemquery->result_array();
			}				
			return array();
		}
		
	}
	
	function populateWishes($foruser)
	{
		// $session_data = $this->session->userdata('logged_in');		
		{ 	//get all items and details pertaining to them, so that we can foreach them onto the page
			$this->db->select('*');
			$this->db->from('wishes');
			$this->db->where('username', $foruser);
			$itemquery = $this->db->get();
			if($itemquery->num_rows() > 0)
			{
				return $itemquery->result_array();
			}				
			return array();
		}
		
	}
	
	function wishExists($itemId, $userName)
	{
		$this->db->select('itemId, userName');
		$this->db->from('wishes');
		$this->db->where('ItemID', $itemId);
		$this->db->where('userName', $userName);
		
		$wishquery = $this->db->get();
		if($wishquery->num_rows() > 0)
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
		
	}
	
	function getItemDetails($ItemID)
	{		
		$this->db->select('*');
		$this->db->from('items');
		$this->db->where('ItemID', $ItemID);
		$singleitem = $this->db->get();
		if($singleitem->num_rows()==1)
		{
			return $singleitem->result();			
		}
		return array();		
	}
	
	function addToWishlist($data)
	{
		return $this->db->insert('wishes', $data);			
	}
	
	function removeFromWishlist($wishId)
	{
		$this->db->where('ID', $wishId);
		$this->db->delete('wishes');
	}
	
}